﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;

namespace Sorter
{
    public partial class Form1 : Form
    {
        string API_key = "d3cf575cfdcb4080be88935b9f470b2a";
        //string url = "https://api.clarifai.com/v2/models/d16f390eb32cad478c7ae150069bd2c6/outputs";
        string url = "https://api.clarifai.com/v2/models/aaa03c23b3724a16a56b629203edc62c/outputs";
        private System.ComponentModel.BackgroundWorker backgroundWorker1;

        public Form1()
        {
            InitializeComponent();
            InitializeBackgroundWorker();
        }

        private void InitializeBackgroundWorker()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            backgroundWorker1.DoWork +=
                new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted +=
                new RunWorkerCompletedEventHandler(
            backgroundWorker1_RunWorkerCompleted);
            backgroundWorker1.ProgressChanged +=
                new ProgressChangedEventHandler(
            backgroundWorker1_ProgressChanged);
            backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // First, handle the case where an exception was thrown.
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else if (e.Cancelled)
            {
                // Next, handle the case where the user canceled 
                // the operation.
                // Note that due to a race condition in 
                // the DoWork event handler, the Cancelled
                // flag may not have been set, even though
                // CancelAsync was called.
                label3.Text = "Ready";
                progressBar1.Value = 0;
            }
            else
            {
                // Finally, handle the case where the operation 
                // succeeded.
                MessageBox.Show("Done");
                label3.Text = "Ready";
                progressBar1.Value = 0;
            }

            // Enable the UpDown control.
            //this.numericUpDown1.Enabled = true;

            // Enable the Start button.
            //startAsyncButton.Enabled = true;

            // Disable the Cancel button.
            //cancelAsyncButton.Enabled = false;
            button1.Enabled = true;
            button2.Enabled = true;
            checkBox4.Enabled = true;
            checkBox5.Enabled = true;
            checkBox6.Enabled = true;
            checkBox7.Enabled = true;
            checkBox1.Enabled = true;
            checkBox2.Enabled = true;
            checkBox3.Enabled = true;
            if (checkBox5.Checked)
            {
                textBox3.Enabled = true;
            }
            else
            {
                textBox3.Enabled = false;
            }

            button3.Text = "Start";
        }

        // This event handler updates the progress bar.
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.progressBar1.Value = e.ProgressPercentage;
            //label3.Text = (e.ProgressPercentage.ToString() + "%");
            label3.Text = (e.UserState.ToString());

            //label3.Text = i + "/" + files.Count() + " files processed";
            //if (i + 1 <= progressBar1.Maximum)
            //{
            //    progressBar1.Value = i + 1; progressBar1.Value = i;
            //}
            //else
            //{
            //    progressBar1.Value = progressBar1.Maximum;
            //}
        }

        // This is the method that does the actual work. For this
        // example, it computes a Fibonacci number and
        // reports progress as it does its work.
        void processFiles(BackgroundWorker worker, DoWorkEventArgs e)
        {
            if (checkBox4.Checked && textBox2.Text == "")
            {
                MessageBox.Show("Please select the correct output folder or uncheck \"Different output folder\"", "Incorrect output folder", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string dir = textBox1.Text;
            DirectoryInfo d = new DirectoryInfo(dir);
            //StringCollection imageExtensions = new StringCollection();
            string[] imageExtensions = { ".png", ".jpg", ".jpeg", ".bmp" };

            //progressBar1.Value = 0;
            //progressBar1.Minimum = 0;
            //progressBar1.Maximum = d.EnumerateFiles().Count();
            //label3.Text = "0/" + d.EnumerateFiles().Count() + " files completed";
            int i = 0;

            //The Overload:System.IO.Directory.EnumerateFiles and Overload: System.IO.Directory.GetFiles methods differ as follows: When you use Overload:System.IO.Directory.EnumerateFiles, you can start enumerating the collection of names before the whole collection is returned; when you use Overload:System.IO.Directory.GetFiles, you must wait for the whole array of names to be returned before you can access the array.Therefore, when you are working with many files and directories, Overload:System.IO.Directory.EnumerateFiles can be more efficient.

            var files = d.GetFiles();

            foreach (var file in files)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    //MessageBox.Show(file.ToString());
                    //добавить чекбоксы моделей
                    //сделать вкладки для разных моделей (модели можно использовать вместе)
                    //выделять измененные или новые файлы при завершении (и при успешном завершении и при отмене)
                    //Что делать с не-jpeg файлами у которых уже есть конвертированные файлы?
                    //сделать так, чтобы теги не повторялись (похоже они и так не повторимы)
                    //иконка - хэштэг?
                    //Thread.Sleep(100);

                    if (imageExtensions.Contains(file.Extension))
                    {
                        System.Drawing.Image old_image = System.Drawing.Image.FromFile(file.FullName);

                        MemoryStream ms = new MemoryStream();
                        ImageFormat imageFormat = null;
                        switch (file.Extension)
                        {
                            case ".png": imageFormat = ImageFormat.Png; break;
                            case ".jpg": imageFormat = ImageFormat.Jpeg; break;
                            case ".jpeg": imageFormat = ImageFormat.Jpeg; break;
                            case ".bmp": imageFormat = ImageFormat.Bmp; break;
                        }

                        if (imageFormat != ImageFormat.Jpeg && !checkBox7.Checked)
                            continue;

                        //if (imageFormat != ImageFormat.Jpeg && )
                        //    continue;

                        old_image.Save(ms, imageFormat);
                        old_image.Dispose();
                        System.Drawing.Image image = System.Drawing.Image.FromStream(ms);
                        string encodedData = Convert.ToBase64String(ms.ToArray());

                        HttpClient client = new HttpClient();
                        HttpRequestMessage request = new HttpRequestMessage()
                        {
                            RequestUri = new Uri(url),
                            Method = HttpMethod.Post,
                        };

                        request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        request.Headers.Add("Authorization", "Key " + API_key);
                        //request.Headers.Add("Content-Type", "application/json");

                        HttpContent json = new StringContent(
                                "{" +
                                    "\"inputs\": [" +
                                        "{" +
                                            "\"data\": {" +
                                                "\"image\": {" +
                                                    "\"base64\": \"" + encodedData + "\"" +
                                                "}" +
                                            "}" +
                                        "}" +
                                    "]" +
                                "}", Encoding.UTF8, "application/json");

                        request.Content = json;

                        var response = client.SendAsync(request).Result;

                        if (!response.IsSuccessStatusCode)
                        {
                            MessageBox.Show("Request returned error: " + response.ReasonPhrase);
                        }

                        string body = response.Content.ReadAsStringAsync().Result.ToString();

                        RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(body);
                        string keywords = "";
                        if (checkBox6.Checked && imageFormat == ImageFormat.Jpeg)
                        {
                            try
                            {
                                PropertyItem propertyItem_old = image.GetPropertyItem(0x9c9e);
                                string old_keywords = Encoding.UTF8.GetString(propertyItem_old.Value);
                                old_keywords = Regex.Replace(old_keywords, "\0", string.Empty) + ";";
                                //MessageBox.Show("Было: " + old_keywords);
                                keywords = old_keywords;
                            }
                            catch
                            {

                            }
                        }
                        foreach (Concept concept in rootObject.outputs[0].data.concepts)
                        {
                            if (concept.value > 0.8)
                            {
                                keywords += concept.name + "; ";
                            }
                        }

                        // повторы удаляем в конце

                        //PropertyItem propertyItem = image.GetPropertyItem(0x9c9e);
                        //string old_keywords = Encoding.UTF8.GetString(propertyItem.Value);
                        //old_keywords = Regex.Replace(old_keywords, "\0", string.Empty);
                        //MessageBox.Show("Было: " + old_keywords);

                        //propertyItem.Value = System.Text.ASCIIEncoding.ASCII.GetBytes(old_keywords + keywords);
                        //propertyItem.Value = Encoding.UTF8.GetBytes("kw1;kw2");
                        //propertyItem.Value = System.Text.UTF8Encoding.UTF8.GetBytes("kw1;kw2");



                        //propertyItem.Value = System.Text.ASCIIEncoding.ASCII.GetBytes("kw1");
                        //image.SetPropertyItem(propertyItem);
                        //image.Save(file.DirectoryName + "\\" + "tagged " + file.Name, image.RawFormat);

                        //PropertyItem propertyItem = image.GetPropertyItem(0x9c9e);
                        PropertyItem propertyItem = image.PropertyItems[0];

                        var text = keywords + char.MinValue;//add \0 at the end of your string
                        propertyItem.Id = 0x9c9e;
                        propertyItem.Value = Encoding.Unicode.GetBytes(text);
                        propertyItem.Len = propertyItem.Value.Length;
                        propertyItem.Type = 1;
                        image.SetPropertyItem(propertyItem);
                        //image.Save(file.FullName);

                        string savePath;
                        if (checkBox4.Checked)
                            savePath = textBox2.Text;
                        else
                            savePath = file.DirectoryName;

                        string saveName;
                        if (checkBox5.Checked)
                            saveName = Path.GetFileNameWithoutExtension(file.FullName) + textBox3.Text;
                        else
                            saveName = Path.GetFileNameWithoutExtension(file.FullName);

                        if (checkBox7.Checked && imageFormat != ImageFormat.Jpeg)
                            saveName += ".jpg";
                        else
                            saveName += file.Extension;

                        if (System.IO.File.Exists(savePath + "\\" + saveName))
                            System.IO.File.Delete(savePath + "\\" + saveName);

                        image.Save(savePath + "\\" + saveName, System.Drawing.Imaging.ImageFormat.Jpeg);


                        i++;
                        backgroundWorker1.ReportProgress((i * 100) / files.Count(), i + "/" + files.Count() + " files processed");
                        //label3.Text = i + "/" + files.Count() + " files processed";
                        //if (i + 1 <= progressBar1.Maximum)
                        //{
                        //    progressBar1.Value = i + 1; progressBar1.Value = i;
                        //}
                        //else
                        //{
                        //    progressBar1.Value = progressBar1.Maximum;
                        //}

                        //using (FileStream fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read))
                        //using (System.Drawing.Image myImage = System.Drawing.Image.FromStream(fs, false, false))
                        //{
                        //40094
                        //PropertyItem propItem = myImage.GetPropertyItem(0x010F);
                        //string s = Encoding.UTF8.GetString(propItem.Value);
                        //MessageBox.Show(s);
                        //BitConverter.ToChar(image.GetPropertyItem(40094).Value.ToString(), 0);

                        //PropertyItem propertyItem = myImage.GetPropertyItem(0x9c9e);

                        //string old_keywords = Encoding.UTF8.GetString(propertyItem.Value);
                        //old_keywords = Regex.Replace(old_keywords, "\0", string.Empty);
                        //MessageBox.Show("Было: " + old_keywords);

                        //propertyItem.Value = System.Text.UTF8Encoding.UTF8.GetBytes("kw1");

                        //myImage.SetPropertyItem(propertyItem);
                        //myImage.Save(file.DirectoryName + "\\" + "tagged " + file.Name, myImage.RawFormat);








                        //}

                        //MessageBox.Show(o);
                        //convert png to jpeg if not jpeg
                    }
                }
            }

            //label3.Text = "All files processed";



            //добавить автоматическую регистрацию и вытягивание ключа

            //if (comboBox1.SelectedItem == "Moderation")
            //{

            //}
        }
        
        // This event handler is where the actual,
        // potentially time-consuming work is done.
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            // Get the BackgroundWorker that raised this event.
            BackgroundWorker worker = sender as BackgroundWorker;

            // Assign the result of the computation
            // to the Result property of the DoWorkEventArgs
            // object. This is will be available to the 
            // RunWorkerCompleted eventhandler.
            processFiles(worker, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog2 = new FolderBrowserDialog();
            if (folderBrowserDialog2.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog2.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                if (backgroundWorker1.WorkerSupportsCancellation == true)
                {
                    // Cancel the asynchronous operation.
                    backgroundWorker1.CancelAsync();
                }
            }
            else
            {
                button1.Enabled = false;
                button2.Enabled = false;
                checkBox4.Enabled = false;
                checkBox5.Enabled = false;
                checkBox6.Enabled = false;
                checkBox7.Enabled = false;
                checkBox1.Enabled = false;
                checkBox2.Enabled = false;
                checkBox3.Enabled = false;
                textBox3.Enabled = false;

                button3.Text = "Cancel";

                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void saveAsJpeg()
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                textBox2.Enabled = true;
                button2.Enabled = true;
            }
            else
            {
                textBox2.Enabled = false;
                button2.Enabled = false;
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
            {
                textBox3.Enabled = true;
            }
            else
            {
                textBox3.Enabled = false;
            }
        }
    }
    public class Status
    {
        public int code { get; set; }
        public string description { get; set; }
    }

    public class Status2
    {
        public int code { get; set; }
        public string description { get; set; }
    }

    public class OutputInfo
    {
        public string message { get; set; }
        public string type { get; set; }
        public string type_ext { get; set; }
    }

    public class Status3
    {
        public int code { get; set; }
        public string description { get; set; }
    }

    public class ModelVersion
    {
        public string id { get; set; }
        public DateTime created_at { get; set; }
#pragma warning disable IDE1006 // Стили именования
        public Status3 status { get; set; }
#pragma warning restore IDE1006 // Стили именования
    }

    public class Model
    {
        public string id { get; set; }
        public string name { get; set; }
        public DateTime created_at { get; set; }
        public string app_id { get; set; }
        public OutputInfo output_info { get; set; }
        public ModelVersion model_version { get; set; }
        public string display_name { get; set; }
    }

    public class Image
    {
        public string url { get; set; }
        public string base64 { get; set; }
    }

    public class Data
    {
        public Image image { get; set; }
    }

    public class Input
    {
        public string id { get; set; }
        public Data data { get; set; }
    }

    public class Concept
    {
        public string id { get; set; }
        public string name { get; set; }
        public double value { get; set; }
        public string app_id { get; set; }
    }

    public class Data2
    {
        public List<Concept> concepts { get; set; }
    }

    public class Output
    {
        public string id { get; set; }
        public Status2 status { get; set; }
        public string created_at { get; set; }
        public Model model { get; set; }
        public Input input { get; set; }
        public Data2 data { get; set; }
    }

    public class RootObject
    {
        public Status status { get; set; }
        public List<Output> outputs { get; set; }
    }
}
